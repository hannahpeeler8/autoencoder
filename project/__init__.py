import numpy as np
from PIL import Image
#from . import models
from . import models as m
#from models import Encoder, Decoder
from torchvision.transforms import ToTensor, Normalize
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
from torch.autograd import Variable
from torch import nn
import torch
from torchvision.transforms import ToPILImage
from pathlib import Path
from torchvision.datasets import CocoDetection

def encode(img):
    """
    Your code here
    img: a 256x256 PIL Image
    """

    img = img.convert(mode='RGB')
    img = ToTensor()(img).unsqueeze(0)
    img = Variable(img)

    x = models.forward_encoder(img)
    x = np.asarray(x.detach())

    return x
    
def decode(x):
    """
    Your code here
    x: a numpy array, <= 8192 Byte
    """
    
    x = torch.from_numpy(x)

    x = models.forward_decoder(x)

    #save_image(x, 'dc_img/return.jpg')

    x = torch.squeeze(x)
    x = ToPILImage(mode="RGB")(x)

    return x

def get_data_loader(path_name, batch_size=1):
    path = Path(path_name)
    
    def _loader():
        for img_path in path.glob('*.jpg'):
            img = Image.open(img_path)
            yield img
            
    return _loader

num_epochs = 30
batch_size = 256
learning_rate = 1e-3

img_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

#dataset = CocoDetection('./data', transform=img_transform, download='True')
#dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

models = m.autoencoder()
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(models.parameters(), lr=learning_rate, weight_decay=1e-5)

data_loader = get_data_loader('./train_data')
for epoch in range(num_epochs):
    #print(epoch)
    for img in data_loader():

        img = img.resize((256, 256), Image.ANTIALIAS)

        img = img.convert(mode='RGB')
        img = ToTensor()(img).unsqueeze(0)
        img = Variable(img)
        # ===================forward=====================
        output = models.train(img)
        Normalize(output, std= 0.1)
        loss = criterion(output, img)
        # ===================backward====================
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

#print("Saving the model")
torch.save(models.state_dict(), 'conv_autoencoder.pth')